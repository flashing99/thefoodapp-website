import 'package:flutter/cupertino.dart';

/// Created by Musa Usman on 26.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class HoverScaleButton extends StatefulWidget {
  final VoidCallback onTap;
  final Widget child;

  const HoverScaleButton({Key key, @required this.onTap, @required this.child})
      : super(key: key);

  @override
  _HoverScaleButtonState createState() => _HoverScaleButtonState();
}

class _HoverScaleButtonState extends State<HoverScaleButton>
    with SingleTickerProviderStateMixin {
  Animation<double> scaleAnimation;
  AnimationController _hoverScaleAnimationController;

  @override
  void initState() {
    _hoverScaleAnimationController = AnimationController(
      vsync: this,
      lowerBound: 0.65,
      upperBound: 1,
      duration: Duration(milliseconds: 200),
      reverseDuration: Duration(milliseconds: 200),
    );

    _hoverScaleAnimationController.addListener(() {
      setState(() {});
    });

    scaleAnimation = CurvedAnimation(
      parent: _hoverScaleAnimationController,
      curve: Curves.easeInOut,
    );

    super.initState();
  }

  @override
  void dispose() {
    _hoverScaleAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: widget.onTap,
        child: MouseRegion(
          onEnter: (_) {
            _hoverScaleAnimationController.forward(
                from: _hoverScaleAnimationController.value);
          },
          onExit: (_) {
            _hoverScaleAnimationController.reverse();
          },
          child: Transform.scale(
            scale: scaleAnimation.value,
            child: widget.child,
          ),
        ),
      );
}
