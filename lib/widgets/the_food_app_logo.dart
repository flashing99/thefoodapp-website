import 'package:flutter/material.dart';
import 'package:the_food_app/utils/app_theme.dart';
import 'package:the_food_app/utils/globals.dart';
import 'package:zap_architecture_flutter/src/helpers.dart';


/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class AppLogo extends StatefulWidget {
  final Color textColor;
  final double size;
  final bool mobileFullLogo;

  const AppLogo({Key key, this.textColor, this.size,this.mobileFullLogo = false}) : super(key: key);

  @override
  _AppLogoState createState() => _AppLogoState();
}

class _AppLogoState extends State<AppLogo> {
  @override
  Widget build(BuildContext context) => Container(
        height: widget.size ?? sizeConfig.height(0.065),
        child: FittedBox(
          fit: BoxFit.fitHeight,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _icon(),
              SizedBox(
                width: sizeConfig.width(.001),
              ),
              if(sizeConfig.isDesktopScreen||widget.mobileFullLogo)
              Text(
                "TheFoodApp.Co",
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: widget.textColor ?? AppTheme.primaryColor,
                    ),
              ),
            ],
          ),
        ),
      );

  _icon() => Icon(
        Icons.whatshot_outlined,
        color: AppTheme.primaryColor,
        size: sizeConfig.height(0.065),
      );
}
