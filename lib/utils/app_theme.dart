import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Created by Musa Usman on 26.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class AppTheme {
  static const primaryColor = const Color(0xFFFC5B1F);

  static const _brightness = Brightness.light;
  static const _backgroundColor = Colors.white;

  static ThemeData light() => ThemeData(
        primaryColor: primaryColor,
        brightness: _brightness,
        backgroundColor: _backgroundColor,
        scaffoldBackgroundColor: _backgroundColor,
        textTheme: _textTheme(),
        appBarTheme: _appBarTheme(),
        splashColor: Colors.white,
        highlightColor: Colors.white,
      );

  static AppBarTheme _appBarTheme() => AppBarTheme(elevation: 0);

  static TextTheme _textTheme() => GoogleFonts.latoTextTheme(
        TextTheme(
          headline2: TextStyle(
            fontWeight: FontWeight.w700,
            color: Color(0xFF262643),
            fontSize: 68,
          ),
          headline6: TextStyle(
            fontWeight: FontWeight.w600,
            color: Color(0xFF262643),
            fontSize: 31,
          ),
          caption: TextStyle(
            color: Colors.grey,
            fontSize: 16,
          ),
        ),
      );
}
