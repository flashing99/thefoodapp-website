part of 'package:the_food_app/pages/landing_page.dart';

/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class _HeaderSection extends StatefulWidget {
  @override
  __HeaderSectionState createState() => __HeaderSectionState();
}

class __HeaderSectionState extends State<_HeaderSection>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) => Stack(
        children: [
          ///Backdrop
          _backdrop(),

          ///Body Content
          _body(),
        ],
      );

  Widget _backdrop() => Positioned.fill(
        child: Container(color: Theme.of(context).backgroundColor),
      );

  Widget _body() => ScreenTypeLayout(
        desktop: _DesktopBodyContent(),
        mobile: _MobileBodyContent(),
      );
}

class _DesktopBodyContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        height: sizeConfig.height(1),
        padding: guidelines.horizontal,
        child: Column(
          children: [
            _AppBar(),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ///Spacing
                        SizedBox(
                          height: sizeConfig.height(.06),
                        ),

                        ///Heading
                        Flexible(
                          child: SizedBox(
                            height: sizeConfig.height(0.2),
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                "Get a mobile app for\nyour restaurant!",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                          ),
                        ),

                        ///Spacing
                        SizedBox(
                          height: sizeConfig.height(.05),
                        ),

                        Row(
                          children: [
                            ///Try a live demo
                            Text(
                              "Download the live demo",
                              style: Theme.of(context).textTheme.headline5,
                            ),

                            ///Icon
                            Container(
                              margin: EdgeInsets.only(
                                top: sizeConfig.height(.005),
                                left: sizeConfig.width(.005),
                              ),
                              child: Transform.rotate(
                                angle: pi,
                                child: Icon(
                                  Icons.keyboard_backspace_sharp,
                                  size: sizeConfig.height(.03),
                                ),
                              ),
                            ),

                            ///Get It On Google Play
                            HoverScaleButton(
                              onTap: () {
                                js.context.callMethod('open', [
                                  'https://play.google.com/store/apps/details?id=com.eatsadeal.merchants'
                                ]);
                              },
                              // child: Image.network(
                              //   'https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png',
                              //   height: sizeConfig.height(.13),
                              // ),
                              child: CachedNetworkImage(
                                imageUrl: "https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png",
                                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                                  height: sizeConfig.height(0.04),
                                  width: sizeConfig.height(0.04),
                                  child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                height: sizeConfig.height(.18), //It overflows after 0.18
                              ),
                            ),
                          ],
                        ),

                        ///Spacing
                        SizedBox(
                          height: sizeConfig.height(.02),
                        ),

                        Row(
                          children: [
                            ///Contact us
                            Text(
                              "Contact us & get your app",
                              style: Theme.of(context).textTheme.headline5,
                            ),

                            ///Icon
                            Container(
                              margin: EdgeInsets.only(
                                top: sizeConfig.height(.005),
                                left: sizeConfig.width(.005),
                              ),
                              child: Transform.rotate(
                                angle: pi,
                                child: Icon(
                                  Icons.keyboard_backspace_sharp,
                                  size: sizeConfig.height(.03),
                                ),
                              ),
                            ),

                            ///Spacing
                            SizedBox(
                              width: sizeConfig.width(.01),
                            ),

                            ///WhatsApp Contact Button
                            HoverScaleButton(
                              onTap: () {
                                js.context.callMethod('alert', [
                                  'Our Official WhatsApp:\n+923249066001\n\nContact us right now to get a stunning new mobile app for your restaurant!'
                                ]);
                              },
                              // child: Image.network(
                              //   "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                              //   height: sizeConfig.height(.1),
                              //   width: sizeConfig.height(.1),
                              // ),
                              child: CachedNetworkImage(
                                imageUrl: "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                                  height: sizeConfig.height(0.04),
                                  width: sizeConfig.height(0.04),
                                  child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                height: sizeConfig.height(.1),
                                width: sizeConfig.height(.1),
                              ),
                            ),

                            ///Mail Contact Button
                            HoverScaleButton(
                              onTap: () {
                                js.context.callMethod(
                                    'open', ['mailto:hello@musausman.com']);
                              },
                              // child: Image.network(
                              //   'https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0',
                              //   height: sizeConfig.height(.1),
                              //   width: sizeConfig.height(.1),
                              // ),
                              child: CachedNetworkImage(
                                imageUrl: "https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0",
                                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                                  height: sizeConfig.height(0.04),
                                  width: sizeConfig.height(0.04),
                                  child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                height: sizeConfig.height(.1),
                                width: sizeConfig.height(.1),
                              ),
                            ),

                            ///Instagram Contact Button
                            HoverScaleButton(
                              onTap: () {
                                js.context.callMethod('open', [
                                  'https://instagram.com/musa.usman.official'
                                ]);
                              },
                              // child: Image.network(
                              //   'https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png',
                              //   height: sizeConfig.height(.11),
                              // ),
                              child: CachedNetworkImage(
                                imageUrl: "https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png",
                                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                                  height: sizeConfig.height(0.04),
                                  width: sizeConfig.height(0.04),
                                  child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error),
                                height: sizeConfig.height(.11),
                              ),
                            ),
                          ],
                        ),

                        ///Spacing
                        SizedBox(
                          height: sizeConfig.height(.075),
                        ),

                        Row(
                          children: [
                            ///Contact Button
                            ElevatedButton(
                              onPressed: () {
                                js.context.callMethod('open', ['https://musausman.com/contact-me/']);
                              },
                              style: ButtonStyle(
                                elevation: MaterialStateProperty.all<double>(0),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        AppTheme.primaryColor),
                                padding: MaterialStateProperty.all<
                                    EdgeInsetsGeometry>(
                                  EdgeInsets.symmetric(
                                    vertical: sizeConfig.height(.025),
                                    horizontal: sizeConfig.width(.021),
                                  ),
                                ),
                                shape:
                                    MaterialStateProperty.all<OutlinedBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                side: MaterialStateProperty.all<BorderSide>(
                                  BorderSide(
                                    color: AppTheme.primaryColor,
                                    width: 2,
                                  ),
                                ),
                              ),
                              child: Text(
                                "Contact Us & Get Your App",
                                style:
                                    Theme.of(context).textTheme.button.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 18,
                                          height: 1.5,
                                        ),
                              ),
                            ),

                            ///Spacing
                            SizedBox(
                              width: sizeConfig.width(.01),
                            ),

                            ///Live Chat Button
                            OutlinedButton(
                              onPressed: () {
                                js.context.callMethod("openTidioChat");
                              },
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all<
                                    EdgeInsetsGeometry>(
                                  EdgeInsets.symmetric(
                                    vertical: sizeConfig.height(.025),
                                    horizontal: sizeConfig.width(.021),
                                  ),
                                ),
                                shape:
                                    MaterialStateProperty.all<OutlinedBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                side: MaterialStateProperty.all<BorderSide>(
                                  BorderSide(
                                    color: AppTheme.primaryColor,
                                    width: 2,
                                  ),
                                ),
                              ),
                              child: Text(
                                "24/7 Live Chat Support",
                                style:
                                    Theme.of(context).textTheme.button.copyWith(
                                          color: AppTheme.primaryColor,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 18,
                                          height: 1.5,
                                        ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      // child: Image.network(
                      //   "https://musausman.com/wp-content/uploads/2020/11/mockup.png",
                      //   height: sizeConfig.height(.81),
                      // ),
                      child: CachedNetworkImage(
                        imageUrl: "https://musausman.com/wp-content/uploads/2020/11/mockup.png",
                        progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                          height: sizeConfig.height(0.04),
                          width: sizeConfig.height(0.04),
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: sizeConfig.height(.81),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

class _MobileBodyContent extends StatelessWidget {

  final String textMessage= 'Hi, I contacted you through your website.\nI need a Retaurant App for my Business. I am looking forward to have a talk with you.';

  @override
  Widget build(BuildContext context) => Container(
        padding: guidelines.horizontal,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            ///AppBar
            _AppBar(),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.07),
            ),

            ///Heading
            Text(
              "Get a mobile app for your restaurant!",
              style: Theme.of(context).textTheme.headline2.copyWith(
                    fontSize: 50,
                  ),
              textAlign: TextAlign.center,
            ),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.05),
            ),

            ///Try a live demo
            Text(
              "Download the live demo!",
              style: Theme.of(context).textTheme.headline5,
            ),

            ///Get It On Google Play
            HoverScaleButton(
              onTap: () {
                js.context.callMethod('open', [
                  'https://play.google.com/store/apps/details?id=com.eatsadeal.merchants'
                ]);
              },
              // child: Image.network(
              //   'https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png',
              //   height: sizeConfig.height(.4),
              // ),
              child: CachedNetworkImage(
                imageUrl: "https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png",
                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                  height: sizeConfig.height(0.04),
                  width: sizeConfig.height(0.04),
                  child: FittedBox(
                    alignment: Alignment.center,
                    fit: BoxFit.contain,
                    child: CircularProgressIndicator(
                      value: downloadProgress.progress
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
                height: sizeConfig.height(.4),
              ),
            ),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.02),
            ),

            ///Contact us
            Text(
              "Contact us & get your app!",
              style: Theme.of(context).textTheme.headline5,
            ),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.02),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ///WhatsApp Contact Button
                HoverScaleButton(
                  onTap: () {
                    js.context.callMethod(
                      'open',
                      ['https://api.whatsapp.com/send?phone=+923249066001&text=$textMessage']);
                  },
                  // child: Image.network(
                  //   "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                  //   height: sizeConfig.height(.19),
                  //   width: sizeConfig.height(.19),
                  // ),
                  child: CachedNetworkImage(
                    imageUrl: "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                    progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                      height: sizeConfig.height(0.04),
                      width: sizeConfig.height(0.04),
                      child: FittedBox(
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        child: CircularProgressIndicator(
                          value: downloadProgress.progress
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    height: sizeConfig.height(.19),
                    width: sizeConfig.height(.19),
                  ),
                ),

                ///Mail Contact Button
                HoverScaleButton(
                  onTap: () {
                    js.context
                        .callMethod('open', ['mailto:hello@musausman.com']);
                  },
                  // child: Image.network(
                  //   'https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0',
                  //   height: sizeConfig.height(.2),
                  //   width: sizeConfig.height(.2),
                  // ),
                  child: CachedNetworkImage(
                    imageUrl: "https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0",
                    progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                      height: sizeConfig.height(0.04),
                      width: sizeConfig.height(0.04),
                      child: FittedBox(
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        child: CircularProgressIndicator(
                          value: downloadProgress.progress
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    height: sizeConfig.height(.2),
                    width: sizeConfig.height(.2),
                  ),
                ),

                ///Instagram Contact Button
                HoverScaleButton(
                  onTap: () {
                    js.context.callMethod(
                        'open', ['https://instagram.com/musa.usman.official']);
                  },
                  // child: Image.network(
                  //   'https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png',
                  //   height: sizeConfig.height(.19),
                  //   width: sizeConfig.height(.19),
                  // ),
                  child: CachedNetworkImage(
                    imageUrl: "https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png",
                    progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                      height: sizeConfig.height(0.04),
                      width: sizeConfig.height(0.04),
                      child: FittedBox(
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        child: CircularProgressIndicator(
                          value: downloadProgress.progress
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    height: sizeConfig.height(.19),
                    width: sizeConfig.height(.19),
                  ),
                ),
              ],
            ),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.045),
            ),

            ///Contact Button
            SizedBox(
              width: double.maxFinite,
              child: RaisedButton(
                onPressed: () {
                  js.context.callMethod('open', ['https://musausman.com/contact-me/']);
                },
                elevation: 0,
                hoverElevation: 0,
                focusElevation: 0,
                highlightElevation: 0,
                color: AppTheme.primaryColor,
                splashColor: Colors.black.withOpacity(.2),
                highlightColor: Colors.black.withOpacity(.1),
                padding: EdgeInsets.symmetric(
                  vertical: sizeConfig.height(.0325),
                  horizontal: sizeConfig.width(.021),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                  side: BorderSide(
                    color: AppTheme.primaryColor,
                    width: 2,
                  ),
                ),
                child: Text(
                  "Contact Us & Get Your App",
                  style: Theme.of(context).textTheme.button.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.w900,
                        fontSize: 18,
                        height: 1.5,
                      ),
                ),
              ),
            ),

            ///Spacing
            SizedBox(
              height: sizeConfig.height(.02),
            ),

            ///Live Chat Button
            SizedBox(
              width: double.maxFinite,
              child: FlatButton(
                onPressed: () {
                  js.context.callMethod("openTidioChat");
                },
                splashColor: Colors.black.withOpacity(.1),
                highlightColor: Colors.black.withOpacity(.02),
                padding: EdgeInsets.symmetric(
                  vertical: sizeConfig.height(.0325),
                  horizontal: sizeConfig.width(.021),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                  side: BorderSide(
                    color: AppTheme.primaryColor,
                    width: 2,
                  ),
                ),
                child: Text(
                  "24/7 Live Chat Support",
                  style: Theme.of(context).textTheme.button.copyWith(
                        color: AppTheme.primaryColor,
                        fontWeight: FontWeight.w900,
                        fontSize: 18,
                        height: 1.5,
                      ),
                ),
              ),
            ),

            SizedBox(height: sizeConfig.height(0.04)),

            ///Header Image
            Container(
              height: sizeConfig.height(.7),
              // child: Image.network(
              //   "https://musausman.com/wp-content/uploads/2020/11/mockup.png",
              //   width: sizeConfig.width(1),
              //   fit: BoxFit.contain,
              //   alignment: Alignment.bottomCenter,
              // ),
              child: CachedNetworkImage(
                imageUrl: "https://musausman.com/wp-content/uploads/2020/11/mockup.png",
                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                  height: sizeConfig.height(0.04),
                  width: sizeConfig.height(0.04),
                  child: FittedBox(
                    alignment: Alignment.center,
                    fit: BoxFit.contain,
                    child: CircularProgressIndicator(
                      value: downloadProgress.progress
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
                width: sizeConfig.width(1),
                fit: BoxFit.contain,
                alignment: Alignment.bottomCenter,
              ),
            ),
          ],
        ),
      );
}

class _AppBar extends StatefulWidget {
  @override
  __AppBarState createState() => __AppBarState();
}

class __AppBarState extends State<_AppBar> {
  @override
  Widget build(BuildContext context) => Container(
        child: ScreenTypeLayout(
          desktop: _desktopAppBar(),
          mobile: _mobileAppBar(),
        ),
      );

  _desktopAppBar() => Container(
        width: double.maxFinite,
        height: kToolbarHeight * 2.3,
        padding: EdgeInsets.only(
          bottom: sizeConfig.height(.015),
          right: sizeConfig.width(.045),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ///App Logo
            Container(
              margin: EdgeInsets.only(
                top: sizeConfig.height(.04),
              ),
              child: AppLogo(
                size: sizeConfig.height(.05),
                textColor: Theme.of(context).textTheme.headline2.color,
              ),
            ),

            ///Gap
            Spacer(),

            ///Contact Info Block
            Container(
              height: kToolbarHeight * 2.3,
              padding: EdgeInsets.only(
                top: sizeConfig.height(.035),
                left: sizeConfig.width(.02),
                right: sizeConfig.width(.02),
                bottom: sizeConfig.height(.035),
              ),
              child: Row(
                children: [
                  ///Phone Icon
                  Container(
                    margin: EdgeInsets.only(
                      right: sizeConfig.width(.01),
                    ),
                    alignment: Alignment.topCenter,
                    child: Icon(
                      LineAwesomeIcons.what_s_app,
                      color: AppTheme.primaryColor,
                      size: sizeConfig.height(.045),
                    ),
                  ),

                  ///Contact Details
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Text(
                            "+92 324 9066001",
                            style:
                                Theme.of(context).textTheme.subtitle1.copyWith(
                                      fontWeight: FontWeight.w800,
                                    ),
                          ),
                        ),
                        Text(
                          "musausman.com",
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(
                    width: sizeConfig.width(.07),
                  ),

                  ///Phone Icon
                  Container(
                    margin: EdgeInsets.only(
                      right: sizeConfig.width(.01),
                    ),
                    alignment: Alignment.topCenter,
                    child: Icon(
                      Icons.call,
                      color: AppTheme.primaryColor,
                      size: sizeConfig.height(.04),
                    ),
                  ),

                  ///Contact Details
                  Container(
                    // width: sizeConfig.width(.125),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "+92 324 9066001",
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                        Text(
                          "hello@musausman.com",
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  _mobileAppBar() => Container(
        margin: EdgeInsets.only(
          top: sizeConfig.height(.03),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: sizeConfig.width(.035),
        ),
        child: Row(
          children: [
            ///App Logo
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: AppLogo(
                  size: sizeConfig.height(.08),
                ),
              ),
            ),

            ///Contact Info Block
            RaisedButton(
              elevation: 0,
              onPressed: () {},
              color: Colors.black,
              splashColor: Colors.white.withOpacity(.175),
              highlightColor: Colors.white.withOpacity(.125),
              padding: EdgeInsets.symmetric(
                vertical: sizeConfig.height(.0215),
                horizontal: sizeConfig.width(.1),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: sizeConfig.height(.0025),
                    ),
                    child: Icon(
                      Icons.call,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: sizeConfig.width(.02),
                  ),
                  Text(
                    "Contact Now",
                    style: Theme.of(context).textTheme.button.copyWith(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

class ScreenTypeLayoutSwitcher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
