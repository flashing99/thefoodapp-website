part of 'package:the_food_app/pages/landing_page.dart';

/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class _FooterBar extends StatefulWidget {
  @override
  __FooterBarState createState() => __FooterBarState();

  static void _openMusaUsmanAndCo() {
    js.context.callMethod("open", ["https://www.musausman.com"]);
  }
}

class __FooterBarState extends State<_FooterBar> {
  bool _highlightLink = false;

  final TapGestureRecognizer _siteTapRecognizer = TapGestureRecognizer()
    ..onTap = _FooterBar._openMusaUsmanAndCo;

  @override
  Widget build(BuildContext context) => Container(
        alignment: Alignment.centerLeft,
        width: double.maxFinite,
        padding: EdgeInsets.only(
          top: sizeConfig.height(.01),
          bottom: sizeConfig.height(.075),
          left: guidelines.left,
          right: guidelines.right,
        ),
        child: sizeConfig.isDesktopScreen
            ? Container(
                height: sizeConfig.height(0.16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ///Meta Info
                    Flexible(
                      child: _metaInfo(),
                    ),
                    ///Copy Rights Info
                    _copyRightsInfo(),
                  ],
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  ///Meta Info
                  Flexible(
                    child: _metaInfo(),
                  ),

                  ///Copy Rights Info
                  _copyRightsInfo(),
                ],
              ),
      );

  _metaInfo() => Container(
        alignment: sizeConfig.isMobileScreen
            ? Alignment.center
            : Alignment.bottomLeft,
        // margin: EdgeInsets.symmetric(horizontal: sizeConfig.width(0.02)),
        child: Column(
          crossAxisAlignment: sizeConfig.isMobileScreen
              ? CrossAxisAlignment.center
              : CrossAxisAlignment.start,
          children: [
            MouseRegion(
              onEnter: _highlightLinkText,
              onExit: _removeHighlightLinkText,
              cursor: SystemMouseCursors.click,
              child: RichText(
                textAlign: sizeConfig.isMobileScreen
                    ? TextAlign.center
                    : TextAlign.left,
                text: TextSpan(
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.grey[600],
                      ),
                  children: [
                    TextSpan(
                      text: "A Product By ",
                    ),
                    TextSpan(
                      text: "Musa Usman & Co.",
                      recognizer: _siteTapRecognizer,
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: _highlightLink ? Colors.white : null,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Text(
                "First Floor, Workify Building. Plot-43, Block L, Johar Town, Lahore.",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption.copyWith(
                      color: Colors.grey[600],
                    ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: sizeConfig.height(.01),
              ),
              child: Text(
                "WhatsApp/Phone Number: +92 324 9066001",
                style: Theme.of(context).textTheme.caption.copyWith(
                      color: Colors.grey[600],
                    ),
              ),
            ),
            Text(
              "Email Address: hello@musausman.com",
              style: Theme.of(context).textTheme.caption.copyWith(
                    color: Colors.grey[600],
                  ),
            ),
          ],
        ),
      );

  _copyRightsInfo() => Container(
        alignment: sizeConfig.isMobileScreen
            ? Alignment.center
            : Alignment.topRight,
        child: Text(
          "All Rights Reserved.\n© 2020 Musa Usman & Co.",
          textAlign: sizeConfig.isMobileScreen
              ? TextAlign.center
              : TextAlign.right,
          style: Theme.of(context).textTheme.caption.copyWith(
                color: Colors.grey[600],
              ),
        ),
      );

  _highlightLinkText(_) {
    setState(() {
      _highlightLink = true;
    });
  }

  _removeHighlightLinkText(_) {
    setState(() {
      _highlightLink = false;
    });
  }
}
