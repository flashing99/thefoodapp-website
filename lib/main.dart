import 'package:flutter/material.dart';
import 'package:the_food_app/pages/landing_page.dart';
import 'package:the_food_app/utils/app_theme.dart';

void main() => runApp(TheFoodAppWebsite());

class TheFoodAppWebsite extends StatefulWidget {
  @override
  _TheFoodAppWebsiteState createState() => _TheFoodAppWebsiteState();
}

class _TheFoodAppWebsiteState extends State<TheFoodAppWebsite> {
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "TheFoodApp - Mobile App For Your Restaurant!",
        theme: AppTheme.light(),
        home: LandingPage(),
      );
}
